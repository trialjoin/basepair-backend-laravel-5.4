<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::guard("admin")->user()){
        return redirect("/admin");
    }
    
    if(Auth::user()){
        return redirect("/user");
    }  
    
    return redirect("/login");
});


Route::get("/admin/login", "Auth\AdminLoginController@showLoginForm");
Route::post("/admin/login", "Auth\AdminLoginController@login");

Route::get("/login", "Auth\LoginController@showLoginForm");
Route::post("/login", "Auth\LoginController@login");

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function(){
    Route::get("/", "AdminController@home");
    Route::get('logout', 'Auth\AdminLoginController@logout');
    //Admins
    Route::get('admins', 'AdminController@adminsList');
    Route::delete('admin/{id}', 'AdminController@adminDelete');
    Route::get('admin/{id?}', 'AdminController@admin');
    Route::post('admin/{id?}', 'AdminController@adminSave'); 
    
    
    //Users
    Route::get('users', 'AdminController@usersList');
    Route::delete('user/{id}', 'AdminController@userDelete');
    Route::get('user/{id?}', 'AdminController@user');
    Route::post('user/{id?}', 'AdminController@userSave'); 
    
    
    Route::get('conditions', 'AdminController@conditionsList');
    Route::post('conditions', 'AdminController@mergeConditions');
    Route::get('condition/{id}', 'AdminController@condition');
    Route::get('condition/{id}/studies', 'AdminController@conditionStudies');
    Route::get('condition/{id}/sponsors', 'AdminController@conditionSponsors');
    Route::get('condition/{id}/facilities', 'AdminController@conditionFacilities');
    Route::get('conditions/statistics', 'AdminController@conditionStatistics');
    Route::post('condition/{id}', 'AdminController@conditionUpdate');
//    Route::post('conditions', ['uses' => 'AdminController@mergeAllConditions', 'as' => 'conditionsmerge']);
    
    
    Route::get('sponsors', 'AdminController@sponsorsList');
    Route::post('sponsors', 'AdminController@mergeSponsors');
    Route::get('sponsor/{id}', 'AdminController@sponsor');
    Route::get('sponsor/{id}/conditions', 'AdminController@sponsorConditions');
    Route::get('sponsor/{id}/facilities', 'AdminController@sponsorFacilities');
    Route::get('sponsor/{id}/studies', 'AdminController@sponsorStudies');
    Route::post('sponsors', ['uses' => 'AdminController@mergeAllSponsors', 'as' => 'sponsorsmerge1']);
    
    
    Route::get('facilities', 'AdminController@facilitiesList');   
    Route::post('facilities', 'AdminController@mergeFacilities');
    Route::get('facility/{id}', 'AdminController@facility');
    Route::post('facility/{id}', 'AdminController@facilityUpdate');
    Route::get('facility/{id}/conditions', 'AdminController@facilityConditions');
    Route::get('facility/{id}/sponsors', 'AdminController@facilitySponsors');
    Route::get('facility/{id}/studies', 'AdminController@facilityStudies');
    Route::get('facility/{id}/contacts', 'AdminController@facilityContacts');    
    Route::post('facility/{id}/contacts', 'AdminController@mergeFacilityContacts');
    Route::get('facility/{id}/contact/{cId}', 'AdminController@facilityContact');
    Route::post('facility/{id}/contact/{cId}', 'ContactController@contactUpdate');
//    Route::post('facilities', ['uses' => 'AdminController@mergeAllFacilities', 'as' => 'facilitiesmerge1']);
    
    Route::get('studies', 'UserController@studiesList');
    Route::get('studies-facilities', 'UserController@studiesListwithFacilities');
        
});


Route::group(['middleware' => 'auth', 'prefix' => 'user'], function(){
    Route::get("/", "UserController@home");
    Route::get('logout', 'Auth\LoginController@logout');    
    
    
    Route::get('facilities', 'UserController@facilitiesList');
    Route::get('facility/{id}', 'UserController@facility');
    Route::get('conditions', 'UserController@conditionsList');
    Route::get('studies', 'UserController@studiesList');
    Route::get('sponsors', 'UserController@sponsorsList');
        
});