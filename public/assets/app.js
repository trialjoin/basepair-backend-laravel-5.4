(function(){
    //Add custom validation method
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = regexp;
            return this.optional(element) || re.test(value);
        },
        'Invalid value'
    );
    
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    var validationRules = {
        
        adminForm: {
            rules: {
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            }
        },
        
        profileForm: {
            rules: {
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            }
        },
    };
    
    
    var $f;
    for(var k in validationRules){        
        if(!validationRules.hasOwnProperty(k)){
            continue;
        }
        $f = $("#"+k);
        if($f.length == 0){
            continue;
        }        
        $f.validate(validationRules[k]);
    }
    
    
    
    //Links to POST
    $(".btnDelete").on("click", function(e){
        e.preventDefault();
        if(!window.confirm("Are you sure?")){
            return;
        }
        
        var url = $(this).attr("href");        
        var token = $('meta[name="csrf-token"]').attr('content');
        $('<form method="post" action="'+url+'"></form>')
                .append($('<input type="hidden" name="_method" value="DELETE"/>'))
                .append($('<input type="hidden" name="_token" value="'+token+'"/>'))
                .appendTo($("body")).submit();
    });
    
    
    //Datepickers
    $(".datePicker").datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        zIndexOffset: 10000
    });
})();