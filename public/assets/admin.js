(function(){    
    $("[data-checkall]").on("change", function(){
        var n = $(this).data("checkall");
        var checked = $(this).is(":checked");
        if(checked){
            $("[name='"+n+"']").prop("checked", true);
        } else {
            $("[name='"+n+"']").removeProp("checked");
        }
                    
    });
})();