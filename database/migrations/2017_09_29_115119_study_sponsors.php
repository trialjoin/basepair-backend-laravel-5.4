<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudySponsors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {             
        Schema::create('study_sponsors', function (Blueprint $table) {            
            $table->integer("study_id");  
            $table->integer("sponsor_id");            
            $table->index("study_id");
            $table->index("sponsor_id");
        });
        
        $sql = "INSERT INTO study_sponsors SELECT studies.id, sponsors.id FROM studies, sponsors ";
        $sql .= "WHERE studies.nct_id = sponsors.nct_id";
        
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
