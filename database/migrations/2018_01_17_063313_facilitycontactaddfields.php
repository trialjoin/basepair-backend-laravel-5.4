<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Facilitycontactaddfields extends Migration
{
    
    protected $connection = 'pgsql';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facility_contacts', function($table)
        {
            $table->string('title')->nullable();
            $table->string('other_email')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('role')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
