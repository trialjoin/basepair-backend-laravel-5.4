<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudyFacilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_facilities', function (Blueprint $table) {            
            $table->integer("study_id");  
            $table->integer("facility_id");            
            $table->index("study_id");
            $table->index("facility_id");
        });
        
        $sql = "INSERT INTO study_facilities SELECT studies.id, facilities.id FROM studies, facilities ";
        $sql .= "WHERE studies.nct_id = facilities.nct_id";
        
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
