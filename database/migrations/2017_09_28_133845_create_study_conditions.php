<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateStudyConditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE studies ADD id SERIAL PRIMARY KEY;');        
        Schema::create('study_conditions', function (Blueprint $table) {            
            $table->integer("study_id");  
            $table->integer("condition_id");            
            $table->index("study_id");
            $table->index("condition_id");
        });
        
        $sql = "INSERT INTO study_conditions SELECT studies.id, conditions.id FROM studies, conditions ";
        $sql .= "WHERE studies.nct_id = conditions.nct_id";
        
        DB::statement($sql);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
