<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FacilityContactStudies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_contact_studies', function (Blueprint $table) {    
            $table->integer("facility_contact_id");            
            $table->integer("study_id");              
            $table->index("study_id");
            $table->index("facility_contact_id");
        });
        
        $sql = "INSERT INTO facility_contact_studies SELECT facility_contacts.id, studies.id FROM facility_contacts, studies ";
        $sql .= "WHERE studies.nct_id = facility_contacts.nct_id";
        
        DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
