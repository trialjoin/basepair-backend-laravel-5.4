<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    public $timestamps = false;
    
    protected $table = "studies";
    
    
    /**
     * Sponsors
     */
    public function sponsors()
    {
        return $this->belongsToMany("\App\Sponsor", "study_sponsors");
    }
    
    /**
     * Conditions
     */
    public function conditions()
    {
        return $this->belongsToMany("\App\Condition", "study_conditions");
    }
    
    /**
     * Facilities
     */
    public function facilities()
    {
        return $this->hasMany("\App\Facility", "nct_id", "nct_id");
    }
    
    public function secondary()
    {
        return Study::join("id_information", "id_information.nct_id", "=", "studies.nct_id")               
        ->where("id_information.id_type", "secondary_id")
        ->distinct();   
    }
}
