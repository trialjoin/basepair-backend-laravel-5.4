<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyFacility extends Model
{
    public $timestamps = false;
    
    protected $table = "study_facilities";
    
    /**
     * Conditions
     */
    public function conditions()
    {
        return $this->belongsToMany("\App\Condition", "study_conditions");
    }
    
    
}
