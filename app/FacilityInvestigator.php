<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityInvestigator extends Model
{
    
    public $timestamps = false;
    
    protected $table = "facility_investigators";
    
    
    /**
     * Facility
     */
    public function facility()
    {
        return $this->belongsTo("\App\Facility", "facility_id");
    }
    
}
