<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Collection;

class Condition extends Model
{
    public $timestamps = false;
    
    /**
     * Studies
     */
    public function studies()
    {
        return $this->belongsToMany("\App\Study", "study_conditions");
    }
    
    /**
     * Facilities
     */
    public function facilities()
    {
       return Facility::join("study_facilities", "study_facilities.facility_id", "=", "facilities.id")
        ->join("study_conditions", "study_conditions.study_id", "=", "study_facilities.study_id")
        ->where("facilities.country", "United States")
        ->whereNull('type')
        ->where("study_conditions.condition_id", $this->id)
        ->distinct();   
    }
    /**
     * Sponsors
     */
    public function sponsors()
    {        
        return Sponsor::join("study_sponsors", "study_sponsors.sponsor_id", "=", "sponsors.id")
                ->join("study_conditions", "study_conditions.study_id", "=", "study_sponsors.study_id")                
                ->where("study_conditions.condition_id", $this->id)
                ->distinct();                
    }
}
