<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    public $timestamps = false;
    
    protected $connection = 'mysql';
    
    
    protected $fillable = [
        'firstname', 
        'lastname', 
        'email', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return trim($this->firstname ." ".$this->lastname);
    }
}
