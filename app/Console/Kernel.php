<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Admin;
use App\User;
use App\Condition;
use App\Study;
use App\Sponsor;
use App\Facility;
use App\FacilityInvestigator;
use App\FacilityContact;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Conditions Merge
        
        $schedule->call(function (){
            $browser_total_raw = DB::raw('count(3) as total');
            $conditions = Condition::getQuery()
                     ->select('name', $browser_total_raw)
                     ->whereNull('merged1')
                     ->groupBy('name')
                     ->pluck('name');

          
            // Pull condition name from collection and build an array
            $conditionsarray = [];
            foreach ($conditions as $s){
                $conditionsarray[] = $s;
            }
            
            foreach ($conditionsarray as $w){
            // Find conditions
            $items = Condition::select("id")
            ->where("name", "=", $w)
            ->get();
            
                $ids = [];
                foreach($items as $item){
                    $ids[] = $item->id;
                }
            
                   
                $mainCondition = null;
                $toRemove = [];
                $studiesIds = [];
                foreach($ids as $id){
                    $c = Condition::find($id);
                    if($c){
                        if($mainCondition === null){
                            $mainCondition = $c;
                        } else {
                            $toRemove[] = $c->id;
                        }                
                        foreach($c->studies as $s){
                            $studiesIds[] = $s->id;
                        }
                    }
                }
                $studiesIds = array_unique($studiesIds);
                Condition::whereIn("id", $toRemove)->delete();
                $mainCondition->studies()->sync($studiesIds);
                $mainCondition->merged1 = 'true';
                $mainCondition->save();
            }   
        })->twiceDaily(1, 13);

         // Facility Merge
        
        $schedule->call(function (){
        
        $browser_total_raw = DB::raw('count(3) as total');
        $facilities = Facility::getQuery()
                 ->select('name', 'city', $browser_total_raw)
                 ->whereNull('merged')
//                 ->where("country", "=", "United States")
                 ->groupBy('name', 'city')
                 ->orderBy('city')
                 ->get();    
        
        // Pull facility's name from collection and build an array
        $facilitiesarray = [];
        foreach ($facilities as $s){
            $facilitiesarray[] = $s;
        }
        
        foreach ($facilitiesarray as $w){
        // Find Facilities
        $items = Facility::select("id")
            ->where("name", "=", $w->name)
            ->where('city', "=", $w->city)
//            ->where("country", "=", "United States")
            ->get();
        
            // Pull id from array
            $ids = [];
            foreach($items as $item){
                $ids[] = $item->id;
            }
            
        $main = null;
        $toRemove = [];
        $studiesIds = [];
        foreach($ids as $id){
            $c = Facility::find($id);
            if($c){
                if($main === null){
                    $main = $c;
                } else {
                    $toRemove[] = $c->id;
                }                
                foreach($c->studies as $s){
                    $studiesIds[] = $s->id;
                }
            }
        }
        
        $main->merged = 'true';
        $main->save();
        
        $studiesIds = array_unique($studiesIds);
        
        Facility::whereIn("id", $toRemove)->delete();
        FacilityContact::whereIn("facility_id", $toRemove)
                ->update([
                    "facility_id" => $main->id
                ]);
        FacilityInvestigator::whereIn("facility_id", $toRemove)
        ->update([
            "facility_id" => $main->id
        ]);
        $main->studies()->sync($studiesIds);
        
        } 
        
        })->twiceDaily(1, 11);
        
        // Sponsors Merge
        
        $schedule->call(function (){
            
            // Find all the duplicate Sponsor names
            $browser_total_raw = DB::raw('count(1) as total');
            $sponsors = Sponsor::getQuery()
                         ->select('name', $browser_total_raw)
                         ->whereNull('merged')
                         ->groupBy('name')
                         ->orderBy('name')
                         ->pluck('name');
        
            // Pull Sponsors name from collection and build an array
            $sponsorsarray = [];
            foreach ($sponsors as $s){
                $sponsorsarray[] = $s;
            }
      
            foreach ($sponsorsarray as $w){
                // Find Sponsor
                $items = Sponsor::select("id")
                    ->where("name", "=", $w)
                    ->get();
            
            // Fetch Sponsor row IDs
            $ids = [];
            foreach($items as $item){
                $ids[] = $item->id;
            }
         
            $main = null;
            $toRemove = [];
            $studiesIds = [];
        
                foreach($ids as $id){
                    $c = Sponsor::find($id);
                    if($c){
                        if($main === null){
                            $main = $c;
                        } else {
                            $toRemove[] = $c->id;
                        }                
                        foreach($c->studies as $s){
                            $studiesIds[] = $s->id;
                        }
                    }
                }

            $studiesIds = array_unique($studiesIds);
            Sponsor::whereIn("id", $toRemove)->delete();
            $main->studies()->sync($studiesIds);
            $main->merged = 'true';
            $main->save();
            }
        
        })->twiceDaily(1, 11);
        
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
