<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MergedCondition extends Model
{
    public $timestamps = false;
    
    protected $table = "merged_conditions";
    
    /**
     * Studies
     */
    public function studies()
    {
        return $this->belongsToMany("\App\Study", "study_sponsors");
    }
    
    
    /**
     * Conditions
     */
    public function conditions()
    {                
        return Condition::join("study_conditions", "study_conditions.condition_id", "=", "conditions.id")
                ->join("study_sponsors", "study_sponsors.study_id", "=", "study_conditions.study_id")
                ->where("study_sponsors.sponsor_id", $this->id)
                ->distinct();                
    }
    
    
    /**
     * Facilities
     */
    public function facilities()
    {                
        return Facility::join("study_facilities", "study_facilities.facility_id", "=", "facilities.id")
                ->join("study_sponsors", "study_sponsors.study_id", "=", "study_facilities.study_id")
                ->where("study_sponsors.sponsor_id", $this->id)
                ->distinct(); 
    }
    
    /**
     * Conditions
     */
    public function getConditions()
    {        
        $ids = [];
        foreach($this->studies as $s){
            $ids[] = $s->id;
        }
        if(!$ids){
            return new Collection();
        }
        
        return Condition::join("study_conditions", "study_conditions.condition_id", "=", "conditions.id")
                ->whereIn("study_conditions.study_id", $ids)
                ->distinct()
                ->get();
    }
    
    
    /**
     * Facilities
     */
    public function getFacilities()
    {        
        $ids = [];
        foreach($this->studies as $s){
            $ids[] = $s->nct_id;
        }
        if(!$ids){
            return new Collection();
        }
        
        return Facility::whereIn("nct_id", $ids)                
                ->get();
    }
}
