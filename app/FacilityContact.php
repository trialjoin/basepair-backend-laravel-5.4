<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityContact extends Model
{
    
    public $timestamps = false;
    
    protected $table = "facility_contacts";
    
    
    /**
     * Studies
     */
    public function studies()
    {
        return $this->belongsToMany("\App\Study", "facility_contact_studies", "facility_contact_id");
    }
    
    /**
     * Facility
     */
    public function facility()
    {
        return $this->belongsTo("\App\Facility", "facility_id");
    }
    
}
