<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Admin;
use App\User;
use App\Condition;
use App\Study;
use App\Sponsor;
use App\Facility;
use App\FacilityContact;

class ContactController extends Controller
{
        public function contactUpdate(Request $request, $id, $cId)
    {
        $contact = FacilityContact::find($cId);
        
        $contact->title = $request->input('title');
        $contact->name = $request->input('name');
        $contact->phone = $request->input('phone');
        $contact->mobile_phone = $request->input('mobile_phone');
        $contact->email = $request->input('email');
        $contact->other_email = $request->input('other_email');

        $contact->save();
        
        return redirect()->back();        
    }
   
}
