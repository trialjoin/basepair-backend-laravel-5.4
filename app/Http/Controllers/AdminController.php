<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Admin;
use App\User;
use App\Condition;
use App\MergedCondition;
use App\Study;
use App\StudyFacility;
use App\Sponsor;
use App\Facility;
use App\FacilityContact;
use App\FacilityInvestigator;
use DB;

class AdminController extends Controller
{
    public function home()
    {
        return view("admin.home");
    }
    
    
    /**
     * Admins list
     * 
     */
    public function adminsList()
    {        
        $admins = Admin::paginate(20);
        
        return view("admin.admins", [
            "items" => $admins,
            "menu" => "admins"
        ]);
    }
    
    
    /**
     * Admin delete
     * 
     * @param int admin id
     */
    public function adminDelete(Request $request, $id)
    {
        
        Admin::where("id", $id)->delete();
        $request->session()->flash('success', 'Admin was deleted successfully');
        return redirect()->back();
    }
    
    
    /**
     * Admin New/Edit
     * 
     * @param int admin id
     */
    public function admin($id = 0)
    {
        $item = null;
        if($id != 0){        
            $item = Admin::find($id);
        }
        return view("admin.admin", [
            "item" => $item,
            "menu" => "admins"
        ]);
    }
    
    
    /**
     * Admin Save
     * 
     * @param int admin id
     */
    public function adminSave(Request $request, $id = 0)
    {   
        $data = $request->only([
            'firstname', 
            'lastname',
            'email', 
            'password'     
        ]);
        
        $validator = Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:mysql.admins,email,'.$id.',id',
        ]);
        
        
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        
        if(isset($data["password"]) && $data["password"] != ""){
            $data["password"] = bcrypt($data["password"]);                   
        } else {
            unset($data["password"]);
        }
        
        
        if($id == 0){      
            
            Admin::create($data);
            $request->session()->flash('success', 'Admin was created successfully');
        } else {            
            Admin::where("id", $id)->update($data);
            $request->session()->flash('success', 'Admin was updated successfully');
        }
        
        return redirect("/admin/admins");
        
    }
    
    
    
    /**
     * Users list
     * 
     */
    public function usersList()
    {        
        $users = User::paginate(20);
        
        return view("admin.users", [
            "items" => $users,
            "menu" => "users"
        ]);
    }
    
    
    /**
     * User delete
     * 
     * @param int id
     */
    public function userDelete(Request $request, $id)
    {
        
        User::where("id", $id)->delete();
        $request->session()->flash('success', 'User was deleted successfully');
        return redirect()->back();
    }
    
    
    /**
     * User New/Edit
     * 
     * @param int id
     */
    public function user($id = 0)
    {
        $item = null;
        if($id != 0){        
            $item = User::find($id);
        }
        return view("admin.user", [
            "item" => $item,
            "menu" => "users"
        ]);
    }
    
    
    /**
     * User Save
     * 
     * @param int user id
     */
    public function userSave(Request $request, $id = 0)
    {   
        $data = $request->only([
            'firstname', 
            'lastname',
            'email', 
            'password'     
        ]);
        
        $validator = Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:mysql.users,email,'.$id.',id',
        ]);
        
        
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        
        if(isset($data["password"]) && $data["password"] != ""){
            $data["password"] = bcrypt($data["password"]);                   
        } else {
            unset($data["password"]);
        }
        
        
        if($id == 0){      
            
            User::create($data);
            $request->session()->flash('success', 'User was created successfully');
        } else {            
            User::where("id", $id)->update($data);
            $request->session()->flash('success', 'User was updated successfully');
        }
        
        return redirect("/admin/users");
        
    }
    
    /**
     * Conditions
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function conditionsList(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "se"
        ]);
        $limit = intval($filter["i"]);
        $limit = $limit ? $limit : 10;
        $search = trim($filter["s"]);
        $items = [];
        if($search){      
            if($filter["se"]){
                $items = Condition::where("name", "=", $search)
                        ->orderBy('merged', 'desc')
                        ->paginate($limit)
                        ->appends($filter);
            } else {
                $items = Condition::where("name", "ilike", $search."%")
                        ->orderBy('merged')
                        ->paginate($limit)                       
                        ->appends($filter);
            }
        }
        
        return view("admin.conditions", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "conditions"            
        ]);
    }
    
    /**
     * Merge conditions
     */
    public function mergeConditions(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "se"
        ]);
        $ids = $request->get("items");
        if(!$ids){
            $all = $request->get("all");
            if($all != "1"){
                return redirect()->back();
            }
            $search = $request->query("s");    
            if($filter["se"]){
                $items = Condition::select("id")
                    ->where("name", "=", $search)
                    ->get();
            } else {
                $items = Condition::select(["id"])
                    ->where("name", "ilike", $search."%")
                    ->get();
            }
            
            $ids = [];
            foreach($items as $item){
                $ids[] = $item->id;
            }                   
            
        }
  
        $studynames = [];
        foreach($ids as $id){
            $studynames[] = $items = Condition::select("name")
            ->where("id", "=", $id)
            ->first();
        }
        
        $studynames1 = [];
        foreach($studynames as $id){
            $studynames1[] = $id->name;
        }
        
//        dd($studynames1);
        
        $mainCondition = null;
        $toRemove = [];
        $studiesIds = [];
        foreach($ids as $id){
            $c = Condition::find($id);
            if($c){
                if($mainCondition === null){
                    $mainCondition = $c;
                } else {
                    $toRemove[] = $c->id;
                }                
                foreach($c->studies as $s){
                    $studiesIds[] = $s->id;
                }
            }
        }
        
        // Save a relationship of merged conditions with main. Remember to set type.
        $mainname = $mainCondition->name;
        foreach($studynames1 as $studyname){
        MergedCondition::insert(['merged' => $studyname, 'main' => $mainname, 'type' => 'condition']);
        }
        
        $studiesIds = array_unique($studiesIds);
        Condition::whereIn("id", $toRemove)->delete();
        $mainCondition->studies()->sync($studiesIds);
	$mainCondition->merged = 'true';
	$mainCondition->save();
        
        
        return redirect()->back();
    }
    
//        public function mergeAllConditions(Request $request)
//    {
//            $browser_total_raw = DB::raw('count(3) as total');
//            $conditions = Condition::getQuery()
//                     ->select('name', $browser_total_raw)
//                     ->whereNull('merged1')
//                     ->groupBy('name')
//                     ->orderBy('name')
//                     ->pluck('name');
//            
//            // Pull condition name from collection and build an array
//            $conditionsarray = [];
//            foreach ($conditions as $s){
//                $conditionsarray[] = $s;
//            }
//            
//            foreach ($conditionsarray as $w){
//            // Find conditions
//            $items = Condition::select("id")
//                ->where("name", "=", $w)
//                ->get();
//            
//                $ids = [];
//                foreach($items as $item){
//                    $ids[] = $item->id;
//                }
//            
//                   
//            $mainCondition = null;
//            $toRemove = [];
//            $studiesIds = [];
//            
//        foreach($ids as $id){
//            $c = Condition::find($id);
//            if($c){
//                if($mainCondition === null){
//                    $mainCondition = $c;
//                } else {
//                    $toRemove[] = $c->id;
//                }                
//                foreach($c->studies as $s){
//                    $studiesIds[] = $s->id;
//                }
//            }
//        }
//                
//        $studiesIds = array_unique($studiesIds);
//        Condition::whereIn("id", $toRemove)->delete();
//        $mainCondition->studies()->sync($studiesIds);
//        $mainCondition->merged1 = 'true';
//        $mainCondition->save();
//        }          
//            
//        return redirect("/admin");
//    }
    
    /**
     * Condition
     */
    public function condition($id)
    {
        $condition = Condition::find($id);
        if(!$condition){
            throw new NotFoundHttpException;
        }
        
        return view("admin.condition", [
            "condition" => $condition,
            "menu" => "conditions"
        ]);
        
    }
    
    /**
     * Update name
     */
    public function conditionUpdate(Request $request, $id)
    {
        $name = $request->get("name");
        if(!$name){
            return redirect()->back();
        }
        $condition = Condition::find($id);
        if(!$condition){
            return redirect()->back();
        }
        
        // Existing condition name to be put in merged column
        $condition_name = $condition->name;
        
        // Merge tracking. Double check "type" to be correct
        MergedCondition::insert(['merged' => $name, 'main' => $condition_name, 'type' => 'condition']);
        
        $condition->name = $name;
        $condition->save();
        
        return redirect()->back();        
    }
    
    
    /**
     * Condition studies
     */
    public function conditionStudies($id)
    {
        $condition = Condition::find($id);
        if(!$condition){
            throw new NotFoundHttpException;
        }
        
        $items = $condition->studies()->where('overall_status', '=', 'Recruiting')->paginate(20);
        
        return view("admin.condition-studies", [
            "condition" => $condition,
            "items" => $items,
            "menu" => "conditions"
        ]);
        
    }
    
        /**
     * Condition studies
     */
    public function conditionStatistics($id)
    {
       
        $items = $condition->studies()->count()->paginate(20);
        dd($items);
        return view("admin.condition-statistics", [
            "condition" => $condition,
            "items" => $items,
            "menu" => "conditions"
        ]);
        
    }
    
        public function conditionFacilities($id)
    {
        $condition = Condition::find($id);
        if(!$condition){
            throw new NotFoundHttpException;
        }
        $items = $condition->facilities()->orderBy('name')->paginate(20);
        
        return view("admin.condition-facilities", [
            "condition" => $condition,
            "items" => $items,
            "menu" => "conditions"
        ]);
        
    }
    
    /**
     * Condition studies
     */
    public function conditionSponsors($id)
    {
        $condition = Condition::find($id);
        if(!$condition){
            throw new NotFoundHttpException;
        }
        $items = $condition->sponsors()->where('agency_class', '=', 'Industry')->paginate(20);
        return view("admin.condition-sponsors", [
            "condition" => $condition,
            "items" => $items,
            "menu" => "conditions"
        ]);
        
    }
    
    
    /**
     * Sponsors
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function sponsorsList(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "se"
        ]);
        $limit = intval($filter["i"]);
        $limit = $limit ? $limit : 10;
        $search = trim($filter["s"]);
        $items = [];
        if($search){            
            if($filter["se"]){
                $items = Sponsor::where("name", "=", $search)
                        ->paginate($limit)
                        ->appends($filter);
            } else {
                $items = Sponsor::where("name", "ilike", $search."%")
                    ->paginate($limit)
                    ->appends($filter);
            }
        }
        
        return view("admin.sponsors", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "sponsors"            
        ]);
    }
    
    /**
     * Merge Sponsors
     */
    public function mergeSponsors(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "se"
        ]);
        
        $ids = $request->get("items");
        if(!$ids){
            $all = $request->get("all");
            if($all != "1"){
                return redirect()->back();
            }
            $search = $request->query("s");   
            if($filter["se"]){
                $items = Sponsor::select("id")
                    ->where("name", "=", $search)
                    ->get();
            } else {
                $items = Sponsor::select("id")
                    ->where("name", "ilike", $search."%")
                    ->get();
            }
            
            $ids = [];
            foreach($items as $item){
                $ids[] = $item->id;
            }
        }
        
        $sponsornames = [];
            foreach($ids as $id){
                $sponsornames[] = $items = Sponsor::select("name")
                ->where("id", "=", $id)
                ->first();
            }
        
        $sponsornames1 = [];
            foreach($sponsornames as $id){
                $sponsornames1[] = $id->name;
            }
        
        $main = null;
        $toRemove = [];
        $studiesIds = [];
        foreach($ids as $id){
            $c = Sponsor::find($id);
            if($c){
                if($main === null){
                    $main = $c;
                } else {
                    $toRemove[] = $c->id;
                }                
                foreach($c->studies as $s){
                    $studiesIds[] = $s->id;
                }
            }
        }
        
        // Save a relationship of merged conditions with main. Remember to set type.
        $mainname = $main->name;
            foreach($sponsornames1 as $sponsorname){
            MergedCondition::insert(['merged' => $sponsorname, 'main' => $mainname, 'type' => 'sponsor']);
            }
        
        $studiesIds = array_unique($studiesIds);
        Sponsor::whereIn("id", $toRemove)->delete();
        $main->merged = 'true';
        $main->studies()->sync($studiesIds); 
        
        return redirect()->back();
    }
    
    public function mergeAllSponsors(Request $request)
    {
                    // Find all the duplicate Sponsor names
            $browser_total_raw = DB::raw('count(1) as total');
            $sponsors = Sponsor::getQuery()
                         ->select('name', $browser_total_raw)
                         ->whereNull('merged')
                         ->groupBy('name')
                         ->orderBy('name')
                         ->pluck('name');
                         
            
            // Pull Sponsors name from collection and build an array
            $sponsorsarray = [];
            foreach ($sponsors as $s){
                $sponsorsarray[] = $s;
            }
            
            foreach ($sponsorsarray as $w){
                // Find Sponsor
                $items = Sponsor::select("id")
                    ->where("name", "=", $w)
                    ->get();
            
            // Fetch Sponsor row IDs
            $ids = [];
            foreach($items as $item){
                $ids[] = $item->id;
            }
         
            $main = null;
            $toRemove = [];
            $studiesIds = [];
        
                foreach($ids as $id){
                    $c = Sponsor::find($id);
                    if($c){
                        if($main === null){
                            $main = $c;
                        } else {
                            $toRemove[] = $c->id;
                        }                
                        foreach($c->studies as $s){
                            $studiesIds[] = $s->id;
                        }
                    }
                }

            $studiesIds = array_unique($studiesIds);
            Sponsor::whereIn("id", $toRemove)->delete();
            $main->studies()->sync($studiesIds);
            $main->merged = 'true';
            $main->save();
            }
        
        return redirect()->back();
              
    }
    /**
     * Sponsor
     */
    public function sponsor($id)
    {
        $sponsor = Sponsor::find($id);
        if(!$sponsor){
            throw new NotFoundHttpException;
        }
        
        return view("admin.sponsor", [
            "sponsor" => $sponsor,
            "menu" => "sponsors"            
        ]);
        
    }
    
    /**
     * Sponsor Conditions
     */
    public function sponsorConditions($id)
    {
        $sponsor = Sponsor::find($id);
        if(!$sponsor){
            throw new NotFoundHttpException;
        }
        $items = $sponsor->conditions()->paginate(20);
        return view("admin.sponsor-conditions", [
            "sponsor" => $sponsor,
            "items" => $items,
            "menu" => "sponsors"
        ]);
        
    }
    
    /**
     * Sponsor Facilities
     */
    public function sponsorFacilities($id)
    {
        $sponsor = Sponsor::find($id);
        if(!$sponsor){
            throw new NotFoundHttpException;
        }
        $items = $sponsor->facilities()->paginate(20);
        return view("admin.sponsor-facilities", [
            "sponsor" => $sponsor,
            "items" => $items,
            "menu" => "sponsors"
        ]);
        
    }
    
    /**
     * Sponsor Studies
     */
    public function sponsorStudies($id)
    {
        $sponsor = Sponsor::find($id);
        if(!$sponsor){
            throw new NotFoundHttpException;
        }
        $items = $sponsor->studies()->paginate(20);
        return view("admin.sponsor-studies", [
            "sponsor" => $sponsor,
            "items" => $items,
            "menu" => "sponsors"
        ]);
    }
    
    
    /**
     * Facilities
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function facilitiesList(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "c",
            "st",
            "nct",
            "sp",
            "se"
        ]);        
        $limit = intval($filter["i"]);
        $limit = $limit ? $limit : 10;
        $search = trim($filter["s"]);
        $city = trim($filter["c"]);
        $state = trim($filter["st"]);
        $nct = trim($filter["nct"]);
        $sponsor = trim($filter["sp"]);
        $items = [];
        if($search || $city || $state || $nct ||$sponsor){            
            $query = Facility::select("*");
            if($search){
                if($filter["se"]){
                    $query->where("facilities.name", "=", $search);
                } else {
                    $query->where("facilities.name", "ilike", $search."%");
                }
            }
            if($state){
                $query->where("facilities.state", "ilike", $state."%");
            }
            if($city){
                $query->where("facilities.city", "ilike", $city."%");
            }
            if($nct){
                $query->join('study_facilities', 'facilities.id', '=', 'study_facilities.facility_id')->where("study_facilities.nct_id", $nct);
//                $query->where("facilities.nct_id", $nct);
            }
 
            $items = $query->where('country', '=', 'United States')->whereNull('type')->orderBy('state')->distinct()->paginate($limit)
                    ->appends($filter);
        }
        
        return view("admin.facilities", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "facilities"            
        ]);
    }
    
    /**
     * Merge Facilities
     */
    public function mergeFacilities(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "c",
            "st",
            "nct",
            "sp",
            "se"
        ]); 
        $ids = $request->get("items");
        if(!$ids){
            $all = $request->get("all");
            if($all != "1"){
                return redirect()->back();
            }
            $search = trim($request->query("s"));
            $city = trim($request->query("c"));
            $state = trim($request->query("st"));
            $query = Facility::select("id");
            if($search){
                if($filter["se"]){
                    $query->where("name", "=", $search);
                } else {
                    $query->where("name", "ilike", $search."%");
                }
            }            
            if($state){
                $query->where("state", "ilike", $state."%");
            }
            if($city){
                $query->where("city", "ilike", $city."%");
            }
            
            $items = $query->get();
            $ids = [];
            foreach($items as $item){
                $ids[] = $item->id;
            }
        }
        
        $main = null;
        $toRemove = [];
        $studiesIds = [];
        foreach($ids as $id){
            $c = Facility::find($id);
            if($c){
                if($main === null){
                    $main = $c;
                } else {
                    $toRemove[] = $c->id;
                }                
                foreach($c->studies as $s){
                    $studiesIds[] = $s->id;
                }
            }
        }        
        $studiesIds = array_unique($studiesIds);
        
        Facility::whereIn("id", $toRemove)->delete();
        FacilityContact::whereIn("facility_id", $toRemove)
                ->update([
                    "facility_id" => $main->id
                ]);
        FacilityInvestigator::whereIn("facility_id", $toRemove)
        ->update([
            "facility_id" => $main->id
        ]);
        $main->studies()->sync($studiesIds);        
        return redirect()->back();
    }
    
//    public function mergeAllFacilities(Request $request){
//        
//        $browser_total_raw = DB::raw('count(3) as total');
//        $facilities = Facility::getQuery()
//                 ->select('name', 'city', $browser_total_raw)
//                 ->whereNull('merged')
//                 ->where("country", "=", "United States")
//                 ->groupBy('name', 'city')
//                 ->limit(2)
//                 ->orderBy('city')
//                 ->get();    
//        
//        
//        // Pull facility's name from collection and build an array
//        $facilitiesarray = [];
//        foreach ($facilities as $s){
//            $facilitiesarray[] = $s;
//        }
//        
//        foreach ($facilitiesarray as $w){
//        // Find Facilities
//        $items = Facility::select("id")
//            ->where("name", "iLIKE", $w->name)
//            ->where('city', "=", $w->city)
//            ->where("country", "=", "United States")
//            ->get();
//        
//            // Pull id from array
//            $ids = [];
//            foreach($items as $item){
//                $ids[] = $item->id;
//            }
//            
//        $main = null;
//        $toRemove = [];
//        $studiesIds = [];
//        foreach($ids as $id){
//            $c = Facility::find($id);
//            if($c){
//                if($main === null){
//                    $main = $c;
//                } else {
//                    $toRemove[] = $c->id;
//                }                
//                foreach($c->studies as $s){
//                    $studiesIds[] = $s->id;
//                }
//            }
//        }
//        
//        $main->merged = 'true';
//        $main->save();
//        
//        $studiesIds = array_unique($studiesIds);
//        
//        Facility::whereIn("id", $toRemove)->delete();
//        FacilityContact::whereIn("facility_id", $toRemove)
//                ->update([
//                    "facility_id" => $main->id
//                ]);
//        FacilityInvestigator::whereIn("facility_id", $toRemove)
//        ->update([
//            "facility_id" => $main->id
//        ]);
//        $main->studies()->sync($studiesIds);
//        
//        }          
//
//        return redirect()->back();
//    }
    /**
     * Facility
     */
    public function facility($id)
    {
        $facility = Facility::find($id);
        if(!$facility){
            throw new NotFoundHttpException;
        }
        
        $items = $facility->contacts()->paginate(20);
        
        return view("admin.facility", [
            "facility" => $facility,
            "items" => $items,
            "menu" => "facilities"           
        ]);
        
    }
    
    
    
    
    /**
     * Update name
     */
    public function facilityUpdate(Request $request, $id)
    {
        
        $name = $request->get("name");
        $city = $request->get("city");
        
        if(!$name){
            return redirect()->back();
        }

        $facility = Facility::find($id);
        if(!$facility){
            return redirect()->back();
        }
        

        $facility->name = $name;
        $facility->city = $request->input('city');
        $facility->website = $request->input('website');
        $facility->save();
        
        return redirect()->back();        
    }
    
    
    /**
     * Facility Conditions
     */
    public function facilityConditions($id)
    {
        $facility = Facility::find($id);
        if(!$facility){
            throw new NotFoundHttpException;
        }
        $items = $facility->conditions()->paginate(20);
        return view("admin.facility-conditions", [
            "facility" => $facility,
            "items" => $items,
            "menu" => "facilities"
        ]);
        
    }
    
    /**
     * Facility Studies
     */
    public function facilityStudies($id)
    {
        $facility = Facility::find($id);
        if(!$facility){
            throw new NotFoundHttpException;
        }
        $items = $facility->studies()->where('overall_status', '!=', 'Completed')->where('overall_status', '!=', 'Terminated')->paginate(20);
        return view("admin.facility-studies", [
            "facility" => $facility,
            "items" => $items,
            "menu" => "facilities"
        ]);
        
    }
    
    /**
     * Facility Sponsors
     */
    public function facilitySponsors($id)
    {
        $facility = Facility::find($id);
        if(!$facility){
            throw new NotFoundHttpException;
        }
        $items = $facility->sponsors()->paginate(20);
        return view("admin.facility-sponsors", [
            "facility" => $facility,
            "items" => $items,
            "menu" => "facilities"
        ]);        
    }
    
    /**
     * Facility Contacts
     */
    public function facilityContacts($id)
    {
        $facility = Facility::find($id);
        if(!$facility){
            throw new NotFoundHttpException;
        }
        $items = $facility->contacts()->paginate(20);
        return view("admin.facility-contacts", [
            "facility" => $facility,
            "items" => $items,
            "menu" => "facilities"
        ]);
    }
    
    
    
    /**
     * Merge Facility Contacts
     */
    public function mergeFacilityContacts(Request $request, $id)
    {
        $ids = $request->get("items");
        if(!$ids){
            return redirect()->back();
        }       
        $main = null;
        $toRemove = [];
        $studiesIds = [];
        foreach($ids as $id){
            $c = FacilityContact::find($id);
            if($c){
                if($main === null){
                    $main = $c;
                } else {
                    $toRemove[] = $c->id;
                }                
                foreach($c->studies as $s){
                    $studiesIds[] = $s->id;
                }
            }
        }        
        $studiesIds = array_unique($studiesIds);
        FacilityContact::whereIn("id", $toRemove)->delete();        
        $main->studies()->sync($studiesIds);        
        return redirect()->back();
    }
    
    
    /**
     * FacilityContact
     */
    public function facilityContact($id, $cId)
    {
        $contact = FacilityContact::find($cId);
        if(!$contact){
            throw new NotFoundHttpException;
        }
        
        return view("admin.facility-contact", [
            "contact" => $contact,
            "menu" => "facilities"           
        ]);
        
    }
   
}
