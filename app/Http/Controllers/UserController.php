<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Admin;
use App\User;
use App\Condition;
use App\Facility;
use App\StudyFacility;
use App\Sponsor;
use App\Study;
use DB;

class UserController extends Controller
{
    public function home()
    {
        return view("user.home");
    }
    
    
    
    /**
     * Conditions
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function conditionsList(Request $request)
    {
        $filter = $request->only([
            "i",
            "s"
        ]);
        $limit = intval($filter["i"]);
        $limit = $limit ? $limit : 10;
        $search = trim($filter["s"]);
        $items = [];
        if($search){            
            $items = Condition::where("name", "ilike", $search."%")
                    ->paginate($limit)
                    ->appends($filter);
        }
        
        return view("user.conditions", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "conditions"            
        ]);
    }
    
    
    /**
     * Facilities
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function facilitiesList(Request $request)
    {
        $filter = $request->only([
            "i",
            "s",
            "c",
            "st"
        ]);
        $limit = intval($filter["i"]);
        $limit = $limit ? $limit : 10;
        $search = trim($filter["s"]);
        $city = trim($filter["c"]);
        $state = trim($filter["st"]);
        $items = [];
        if($search || $city || $state){            
            $query = Facility::select("*");
            if($search){
                $query->where("name", "ilike", $search."%");
            }
            if($state){
                $query->where("state", "ilike", $state."%");
            }
            if($city){
                $query->where("city", "ilike", $city."%");
            }
            
            $items = $query->paginate($limit)
                    ->appends($filter);
        }
        
        return view("user.facilities", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "facilities"            
        ]);
    }
    
    
    /**
     * Facility
     */
    public function facility($id)
    {
        $facility = Facility::find($id);
        if(!$facility){
            throw new NotFoundHttpException;
        }
        
        return view("user.facility", [
            "facility" => $facility
        ]);
        
    }
    
    
    /**
     * Sponsors
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function sponsorsList(Request $request)
    {
        $filter = $request->only([
            "i",
            "s"
        ]);
        $limit = intval($filter["i"]);
        $limit = $limit ? $limit : 10;
        $search = trim($filter["s"]);
        $items = [];
        if($search){            
            $items = Sponsor::where("name", "ilike", $search."%")
                    ->paginate($limit)
                    ->appends($filter);
        }
        
        return view("user.sponsors", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "sponsors"            
        ]);
    }
    
    
    /**
     * Studies
     */
    public function studiesList(Request $request)
    {
        $filter = [];
        
        $items = Study::paginate(20)
                ->appends($filter);
        
        
        return view("user.studies", [
            "filter" => $filter,
            "items" => $items,
            "menu" => "studies"            
        ]);
    }
    
       public function studiesListwithFacilities(Request $request)
    {
        $browser_total_raw = DB::raw('count(3) as total');
        $studies = StudyFacility::getQuery()
                 ->select('study_id', $browser_total_raw)
                 ->groupBy('study_id')
                 ->get(); 
        
        $studyids = [];
        foreach ($studies as $study){
            $studyids[] = $study->study_id;
        }
       
        $items = [];
        foreach  ($studyids as $si){
            $items[] = Study::find($si);
        }
        
        return view("user.studies", [
            "items" => $items,
            "menu" => "studies"            
        ]);
    } 
    

    
   
}
