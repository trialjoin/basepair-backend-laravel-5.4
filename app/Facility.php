<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    
    public $timestamps = false;
    
    protected $table = "facilities";
    
    
    /**
     * Studies
     */
    public function studies()
    {
        return $this->belongsToMany("\App\Study", "study_facilities", "facility_id");
    }
    
    /**
     * Contacts
     */    
    public function contacts()
    {
        return $this->hasMany("\App\FacilityContact");
    }
    
    public function nct()
    {
        return $this->hasMany("\App\StudyFacility", "study_facilities");
    }
    
    
    
    /**
     * Conditions
     */
    public function conditions()
    {                
        return Condition::join("study_conditions", "study_conditions.condition_id", "=", "conditions.id")
                ->join("study_facilities", "study_facilities.study_id", "=", "study_conditions.study_id")
                ->where("study_facilities.facility_id", $this->id)
                ->distinct();                
    }
    
    
    /**
     * Sponsors
     */
    public function sponsors()
    {                
        return Sponsor::join("study_sponsors", "study_sponsors.sponsor_id", "=", "sponsors.id")
                ->join("study_facilities", "study_facilities.study_id", "=", "study_sponsors.study_id")
                ->where("study_facilities.facility_id", $this->id)
                ->distinct();                
    }
    
    /**
     * Conditions
     */
    public function getConditions()
    {        
        $ids = [];
        foreach($this->studies as $s){
            $ids[] = $s->id;
        }
        if(!$ids){
            return new Collection();
        }
        
        return Condition::join("study_conditions", "study_conditions.condition_id", "=", "conditions.id")
                ->whereIn("study_conditions.study_id", $ids)
                ->distinct()
                ->get();
    }
    
    /**
     * Sponsors
     */
    public function getSponsors()
    {        
        $ids = [];
        foreach($this->studies as $s){
            $ids[] = $s->id;
        }
        if(!$ids){
            return new Collection();
        }
        
        return Sponsor::join("study_sponsors", "study_sponsors.sponsor_id", "=", "sponsors.id")
                ->whereIn("study_sponsors.study_id", $ids)
                ->distinct()
                ->get();
    }
    
}
