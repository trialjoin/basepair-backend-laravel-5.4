@extends('layouts.app')

@section('content')
<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="" method="post">
    {{ csrf_field() }}
    <h3 class="form-title">Login to your account</h3>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span> Enter email and password. </span>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>        
            @foreach ($errors->all() as $error)
            <div><span>{{ $error }}</span></div>                
            @endforeach            
        </div>
    @endif
    
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> 
        </div>
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> 
        </div>        
    </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">Login</button>                
    </div>
    <div class="forget-password">
        <h4>Forgot your password ?</h4>
        <p> no worries, click
            <a href="/forgot"> here </a> to reset your password. </p>
    </div>
    
</form>
<!-- END LOGIN FORM -->

@endsection