@extends('layouts.app')

@section('content')
<div class="logo"><img src="/assets/images/logo.png"/></div>
<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" action="" method="post">
    {{ csrf_field() }}
    <h3>Forget Password ?</h3>
    <p> Enter your e-mail address below to reset your password. </p>
    
    @if (Session::has('success'))
    <div class="alert alert-success">{!! session('success') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-error">{!! session('error') !!}</div>
    @endif   
        
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
        </div>
        @if ($errors->has('email'))
        <span class="help-block">{{ $errors->first('email') }}</span>
        @endif
    </div>
    <div class="form-actions">
        <a href="/login" class="btn red btn-outline">Back</a>
        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->


@endsection