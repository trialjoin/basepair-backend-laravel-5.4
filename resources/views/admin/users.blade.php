@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>Users</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <a href="/admin/user" class="btn green" aria-expanded="false">
                <i class="fa fa-plus"></i> User                
            </a>            
        </div>
    </div>
</div>

<h1 class="page-title">Users</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <div class="bootstrap-table">
            
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->getName() }}</td>
                        <td>{{ $item->email }}</td>
                        <td>                                                                                    
                            <div class="btn-group">
                                <button class="btn green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url("/admin/user", ["id" => $item->id])}}">
                                            <i class="icon-pencil"></i> Edit</a>                                        
                                    </li>
                                    <li>
                                        <a href="{{ url("/admin/user", ["id" => $item->id])}}" class="btnDelete">
                                            <i class="icon-trash"></i> Delete </a>
                                    </li>                                   
                                </ul>
                            </div>
                        </td>
                    </tr>            
                    @endforeach 
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<nav>
    {{ $items->links() }}        
</nav>

@endsection
