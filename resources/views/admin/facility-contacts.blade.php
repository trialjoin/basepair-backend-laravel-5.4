@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/{{$prefix}}">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/{{$prefix}}/facilities">Facilities</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/{{$prefix}}/facility/{{$facility->id}}">{{$facility->name}}</a>
            <i class="fa fa-circle"></i>            
        </li>
        <li>
            <span>Contacts</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$facility->name}} Contacts</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">  
        <form action="" method="post">
                {{csrf_field()}}
            <table class="table">
                <thead>
                    <tr>
                        <th><label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" value="1" name="checkall" data-checkall="items[]">
                                <span></span>
                            </label>       
                        </th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Studies</th>
                        <th>Actions<br/>
                            <button type="submit" name="merge" class="btn btn-primary">Merge Selected</button>
                        </th>
                    </tr>
                </thead>
                <tbody>                
                    @foreach($items as $c)
                    <tr>
                        <td><label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" value="{{ $c->id }}" name="items[]">
                                <span></span>
                            </label>
                        </td>
                        <td><a href="{{ url("/admin/facility/".$facility->id."/contact/".$c->id)}}">{{$c->name}}</a></td>
                        <td>{{$c->email}}</td>
                        <td>{{$c->phone}}</td>
                        <td><a href="{{ url("/admin/facility/".$facility->id."/contact/".$c->id)}}">{{$c->studies()->count()}}</a></td>
                        <td></td>
                    </tr>
                    @endforeach         
                </tbody>
            </table>
        </form>       
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif
@endsection
