@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>Sponsors</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">Sponsors</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <div class="bootstrap-table">
            <div>
                {!! Form::model($filter, [ 'method' => 'get']) !!}
                    <div class="pull-left form-inline">
                        <div class="form-group">
                            <label class="control-label">Show</label>
                            {!! Form::select('i', [10 => 10, 25 => 25, 50 => 50,100 => 100] , null, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) !!}                                    
                        </div>
                    </div>
                    
                    <div class="pull-right form-inline">
                        <div class="form-group">
                            <label class="control-label">Search:</label>
                            {!! Form::text('s', null, ['class' => 'form-control']) !!}                
                        </div>
                        <div class="form-group">
                            <label class="mt-checkbox mt-checkbox-outline">
                                {!! Form::checkbox('se', 1) !!} Exact match                                            
                                <span></span>
                            </label>                            
                        </div>
                    </div>
                </form>
            </div>
            <form action="" method="post">
                {{csrf_field()}}
                <table class="table">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" value="1" name="checkall" data-checkall="items[]">
                                    <span></span>
                                </label>       
                            </th>
                            <th>Name</th>  
                            <th>Actions<br/>
                                @if(count($items))
                                <button type="submit" name="merge" class="btn btn-primary">Merge Selected</button>
                                <button type="submit" name="all" value="1" class="btn btn-primary">Merge All</button>
                                @endif
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($items))
                            @foreach ($items as $item)
                            <tr>
                                <td><label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" value="{{ $item->id }}" name="items[]">
                                    <span></span>
                                </label>
                                </td>
                                <td>{{$item->name}}</td>                            
                                <td><a href="/admin/sponsor/{{$item->id}}" class="btn btn-primary">View</a></td>
                            </tr>            
                            @endforeach 
                        @else
                        <tr><td colspan="3" class="text-center">No data</td></tr>
                        @endif
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif

@endsection
