@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/{{$prefix}}">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/{{$prefix}}/facilities">Facilities</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/{{$prefix}}/facility/{{$contact->facility_id}}">{{$contact->facility->name}}</a>
            <i class="fa fa-circle"></i>            
        </li>
        <li>
            <span>{{ $contact->name }}</span>          
        </li>
    </ul>    
</div>

    <h1 class="page-title">{{$contact->name}}</h1>
@include('common.flash-message')
<!-- BEGIN CONTACT INFO-->
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            Information </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="container">
            <div class="row"> 
                <div class="col-md-3">  
            <form method="post" action="">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Name:</label>
                        <div class="input-group input-group">
                            <input class="form-control" name="name" value="{{$contact->name}}"/>
                        </div>
                    </div>      
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Title:</label>
                        <div class="input-group input-group">
                            <input class="form-control" name="title" value="{{$contact->title}}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Email:</label>
                        <div class="input-group input-group">
                            <input class="form-control" name="email" value="{{$contact->email}}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Other Email:</label>
                        <div class="input-group input-group">
                            <input class="form-control" name="other_email" value="{{$contact->other_email}}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Phone:</label>
                        <div class="input-group input-group">
                            <input class="form-control" id="mask_phone" name="phone" value="{{$contact->phone}}" type="text"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Mobile Phone:</label>
                        <div class="input-group input-group">
                            <input class="form-control" name="mobile_phone" value="{{$contact->mobile_phone}}"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn green py-2" type="submit">Update</button>
                    <br>
                    <br>
                </div>    
              </form>
            </div>
        </div>
    </div>    
</div>
<br>
<!-- END CONTACT INFO-->

<!-- BEGIN STUDIES LIST-->
<div class="portlet box blue">
    
    <div class="portlet-title">
        <div class="caption">
        Studies 
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    
    <div class="portlet-body form">
     <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>NCT</th>
                    <th>Acronym</th>
                    <th>Status</th>
                    <th>Completion Date</th>
                </tr>
            </thead>
            <tbody> 
                @foreach($contact->studies as $s)
                <tr>
                    <td>{{$s->brief_title}}</td>
                    <td>{{$s->nct_id}}</td>
                    <td>{{$s->acronym}}</td>
                    <td>{{$s->overall_status}}</td>
                    <td>{{$s->completion_month_year}}</td>
                </tr> 
                @endforeach
            </tbody>
    </table>   
    </div>
    
</div>

<!-- END STUDIES LIST-->

@endsection
