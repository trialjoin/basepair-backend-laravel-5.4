@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
}?>

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/{{$prefix}}">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/{{$prefix}}/facilities">Facilities</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>{{$facility->name}}</span>
        </li>
    </ul>    
</div>

<center><h1 class="page-title">{{$facility->name}}</h1></center>


@endsection
