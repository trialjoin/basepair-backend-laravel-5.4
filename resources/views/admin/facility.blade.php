@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/{{$prefix}}">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/{{$prefix}}/facilities">Facilities</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>{{$facility->name}}</span>
        </li>
    </ul>    
</div>

<center><h1>{{$facility->name}}</h1></center>
@include('common.flash-message')
<a href="https://trialjoin.agilecrm.com/#contacts/search/{{$facility->name}}" class="btn btn-primary">AgileCRM</a>
<br><br>
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet box blue">
    
    <div class="portlet-title">
        <div class="caption">
        Information 
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    
    <div class="portlet-body form">
        <div class="container">
        <div class="row">
            
            <div class="col-md-3">   
            <form method="post" action="">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Facility:</label>
                    <div class="input-group">
                        <input class="form-control" name="name" value="{{$facility->name}}"/>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label>Website:</label>
                    <div class="input-group input-group">
                        <input class="form-control" name="website" value="{{$facility->website}}"/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>City:</label>
                    <div class="input-group">
                        <input class="form-control" name="city" value="{{$facility->city}}"/>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label>State:</label>
                    <div class="input-group input-group">
                        <input class="form-control" name="state" value="{{$facility->state}}"/>
                    </div>
                </div>
            </div>
                       
            <div class="col-md-6">
                <div class="form-group">
                    <label>Address:</label>
                    <div class="input-group input-group">
                        <input class="form-control" name="address" value="{{$facility->address}}"/>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>Address2:</label>
                    <div class="input-group input-group">
                        <input class="form-control" name="address2" value="{{$facility->address2}}"/>
                    </div>
                </div>
            </div>
            

            <div class="col-md-12">
                <div class="form-group">
                        <span class="input-group-btn">
                            <button class="btn green" type="submit">Update</button>
                        </span>
                </div>
            </div>
            
          </form>
        </div>  
    </div>
</div> 
</div>

<!-- Facility Stats -->
<br>
<div class="portlet box blue">
    
    <div class="portlet-title">
        <div class="caption">
        Facility Statistics
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    
    <div class="portlet-body form">
        <div class="container">
            <div class="row">
                
                <div class="col-md-3">
                    <p><strong>Conditions:</strong> <a href="/admin/facility/{{$facility->id}}/conditions">{{$facility->conditions()->count()}}</a></p>
                </div>
                <div class="col-md-3">
                    <p><strong>Sponsors:</strong> <a href="/admin/facility/{{$facility->id}}/sponsors">{{$facility->sponsors()->count()}}</a></p>
                </div>
                <div class="col-md-3">
                    <p><strong>Studies:</strong> <a href="/admin/facility/{{$facility->id}}/studies">{{$facility->studies()->count()}}</a></p>
                </div>
                <div class="col-md-3">
                    <p><strong>Contacts:</strong> <a href="/admin/facility/{{$facility->id}}/contacts">{{$facility->contacts()->count()}}</a></p>            
                </div>

            </div>  
        </div>
    </div> 
    
</div>

<!-- Facility Staff Information-->
<br>
<div class="portlet box blue">
    
    <div class="portlet-title">
        <div class="caption">
        Facility Staff ( {{$facility->contacts()->count()}} )
        </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    
    <div class="portlet-body form">
        <div class="container">
            <div class="row">
        <form action="" method="post">
                {{csrf_field()}}
            <table class="table">
                <thead>
                    <tr>
                        <th><label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" value="1" name="checkall" data-checkall="items[]">
                                <span></span>
                            </label>       
                        </th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Studies</th>
                        <th>Actions<br/>
                            <button type="submit" name="merge" class="btn btn-primary">Merge Selected</button>
                        </th>
                    </tr>
                </thead>
                <tbody>                
                    @foreach($items as $c)
                    <tr>
                        <td><label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" value="{{ $c->id }}" name="items[]">
                                <span></span>
                            </label>
                        </td>
                        <td><a href="{{ url("/admin/facility/".$facility->id."/contact/".$c->id)}}">{{$c->name}}</a></td>
                        <td>{{$c->title}}</td>
                        <td>{{$c->email}}</td>
                        <td>{{$c->phone}}</td>
                        <td><a href="{{ url("/admin/facility/".$facility->id."/contact/".$c->id)}}">{{$c->studies()->count()}}</a></td>
                        <td></td>
                    </tr>
                    @endforeach         
                </tbody>
            </table>
        </form>     

            </div>  
        </div>
    </div> 
</div>

<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif
@endsection
