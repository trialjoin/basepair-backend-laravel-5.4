@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/admin/conditions">Conditions</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/admin/condition/{{$condition->id}}">{{$condition->name}}</a>
            <i class="fa fa-circle"></i>            
        </li>
        <li>
            <span>Sponsors</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$condition->name}} Facilities</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>City</th> 
                    <th>State</th>                     
                </tr>
            </thead>
            <tbody> 
                @foreach($items as $s)
                <tr>
                    <td><a href="/admin/facility/{{$s->id}}">{{$s->name}}</a></td>
                    <td>{{$s->city}}</td>
                    <td>{{$s->state}}</td>
                </tr> 
                @endforeach
            </tbody>
        </table>        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif


@endsection
