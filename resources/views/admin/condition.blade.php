@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/admin/conditions">Conditions</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>{{$condition->name}}</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$condition->name}}</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <form method="post" action="">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label">Name</label>
                <input class="form-control" name="name" value="{{$condition->name}}"/>
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>
        <p class="margin-top-20"><strong>Studies:</strong> <a href="/admin/condition/{{$condition->id}}/studies">{{ $condition->studies()->count() }}</a></p>
        
        <p class="margin-top-20"><strong>Sponsors:</strong> <a href="/admin/condition/{{$condition->id}}/sponsors">{{ $condition->sponsors()->count() }}</a></p>
        
        <p class="margin-top-20"><strong>Facilities:</strong> <a href="/admin/condition/{{$condition->id}}/facilities">{{ $condition->facilities()->count() }}</a></p>
        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

@endsection
