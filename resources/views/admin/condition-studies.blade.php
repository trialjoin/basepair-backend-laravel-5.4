@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/admin/conditions">Conditions</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/admin/condition/{{$condition->id}}">{{$condition->name}}</a>
            <i class="fa fa-circle"></i>            
        </li>
        <li>
            <span>Studies</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$condition->name}} Studies</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Facilities</th>
                    <th>Acronym</th>
                    <th>NCT</th>
                    <th>Sponsor</th>
                </tr>
            </thead>
            <tbody> 
                @foreach($items as $s)
                <tr>
                    <td>{{$s->brief_title}}</td>
                    <td><a href="/admin/facilities?c=&st=&nct={{$s->nct_id}}&sp=&s=&i=10">View</a></td>
                    <td>{{$s->acryonym}}</td>
                    <td>{{$s->nct_id}}</td>
                    <td>{{$s->source}}</td>
                </tr> 
                @endforeach
            </tbody>
        </table>        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif


@endsection
