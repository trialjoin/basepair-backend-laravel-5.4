@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/admin/sponsors">Sponsors</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>{{$sponsor->name}}</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$sponsor->name}}</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">        
        <p class="margin-top-20"><strong>Conditions:</strong> <a href="/admin/sponsor/{{$sponsor->id}}/conditions">{{$sponsor->conditions()->count()}}</a></p>
        <p class="margin-top-20"><strong>Facilities:</strong> <a href="/admin/sponsor/{{$sponsor->id}}/facilities">{{$sponsor->facilities()->count()}}</a></p>
        <p class="margin-top-20"><strong>Studies:</strong> <a href="/admin/sponsor/{{$sponsor->id}}/studies">{{$sponsor->studies()->count()}}</a></p>
        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

@endsection
