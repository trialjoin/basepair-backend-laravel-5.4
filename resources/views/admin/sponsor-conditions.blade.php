@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/admin/sponsors">Sponsors</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/admin/sponsor/{{$sponsor->id}}">{{$sponsor->name}}</a>
            <i class="fa fa-circle"></i>            
        </li>
        <li>
            <span>Conditions</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$sponsor->name}} Conditions</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">  
        <table class="table">
            <thead>
                <tr>                    
                    <th>Name</th>                    
                </tr>
            </thead>
            <tbody>                
                @foreach($items as $c)
                <tr>                    
                    <td>{{$c->name}}</td>                    
                </tr>
                @endforeach         
            </tbody>
        </table>
        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif
@endsection
