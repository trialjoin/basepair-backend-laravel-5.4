@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>Facilities</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">Facilities</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <div class="bootstrap-table">
            <div>
                {!! Form::model($filter, [ 'method' => 'get']) !!}
                    <div class="row">
                        <div class="col-md-2">                            
                            <div class="form-group">
                                <label class="control-label">City:</label>
                                {!! Form::text('c', null, ['class' => 'form-control']) !!}                
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">State:</label>
                                {!! Form::text('st', null, ['class' => 'form-control']) !!}                
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">NCT:</label>
                                {!! Form::text('nct', null, ['class' => 'form-control']) !!}                
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Sponsor:</label>
                                {!! Form::text('sp', null, ['class' => 'form-control']) !!}                
                            </div>
                        </div>
                        <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Search:</label>
                            {!! Form::text('s', null, ['class' => 'form-control']) !!}                
                        </div>
                        <div class="form-group">
                            <label class="mt-checkbox mt-checkbox-outline">
                                {!! Form::checkbox('se', 1) !!} Exact match                                            
                                <span></span>
                            </label>                        
                        </div>
                        </div>
                        <div class="col-md-1 text-right">                        
                            <button type="submit" class="btn btn-primary">Search</button>
                            <a href="" class="btn btn-danger">Reset</a>
                        </div>    

                    </div>
                    <div class="pull-left form-inline">
                        <div class="form-group">
                            <label class="control-label">Show</label>
                            {!! Form::select('i', [10 => 10, 25 => 25, 50 => 50,100 => 100] , null, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) !!}                                    
                        </div>
                    </div>

                </form>
            </div>
            <form action="" method="post">
                {{csrf_field()}}
                <table class="table">
                    <thead>
                        <tr>
                            <th><label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" value="1" name="checkall" data-checkall="items[]">
                                    <span></span>
                                </label>       
                            </th>
                            <th>Name</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Studies</th>
                            <th>Actions<br/>
                                @if(count($items))
                                <button type="submit" name="merge" class="btn btn-primary">Merge Selected</button>
                                <button type="submit" name="all" value="1" class="btn btn-primary">Merge All</button>
                                @endif
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($items))
                            @foreach ($items as $item)
                            <tr>
                                <td><label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" value="{{ $item->id }}" name="items[]">
                                    <span></span>
                                </label>
                                </td>
                                <td><a href="/admin/facility/{{$item->id}}">{{$item->name}}</a></td>                        
                                <td>{{$item->city}}</td>                        
                                <td>{{$item->state}}</td>
                                <td><a href="/admin/facility/{{ $item->id }}/studies">{{$item->studies->count()}}</a></td>
                                <td><a href="/admin/facility/{{$item->id}}" class="btn btn-primary">View</a><a href="https://trialjoin.agilecrm.com/#contacts/search/{{$item->name}}" class="btn btn-primary">AgileCRM</a></td>
                            </tr>            
                            @endforeach 
                        @else
                        <tr><td colspan="3" class="text-center">No data</td></tr>
                        @endif
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif

@endsection
