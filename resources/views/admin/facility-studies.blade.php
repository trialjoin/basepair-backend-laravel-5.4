@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/{{$prefix}}">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/{{$prefix}}/facilities">Facilities</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/{{$prefix}}/facility/{{$facility->id}}">{{$facility->name}}</a>
            <i class="fa fa-circle"></i>            
        </li>
        <li>
            <span>Studies</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$facility->name}} Studies</h1><a href="https://trialjoin.agilecrm.com/#contacts/search/{{$facility->name}}">AgileCRM</a>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">  
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Overall Status</th>
                    <th>Completion Date</th>
                    <th>NCT</th>
                    <th>Facilities</th>
                </tr>
            </thead>
            <tbody> 
                @foreach($items as $s)
                <tr>
                    <td>{{$s->brief_title}}</td>
                    <td>{{$s->completion_month_year}}</td>
                    <td>{{$s->overall_status}}</td>
                    <td><a href="https://clinicaltrials.gov/ct2/show/{{$s->nct_id}}">{{$s->nct_id}}</a></td>
                    <td><a href="/admin/facilities?c=&st=&nct={{$s->nct_id}}&sp=&s=&i=10">View</a></td>
                </tr> 
                @endforeach
            </tbody>
        </table>        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif
@endsection
