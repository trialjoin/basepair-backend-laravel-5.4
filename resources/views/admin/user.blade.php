@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/admin/users">Users</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>User</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">User</h1>



<div class="portlet light bordered">    
    <div class="portlet-body form">
        
        {!! Form::model($item, [ 'id' => 'adminForm']) !!}
            <div class="form-body">
                <input type="text" name="fakeemail" value="" class="hidden"/>
                <input type="password" name="fakepassword" value="" class="hidden"/>
                
                <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                    <label>Firstname</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </span>                        
                        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
                    </div>
                    @if ($errors->has('firstname'))
                    <span class="help-block">{{ $errors->first('firstname') }}</span>
                    @endif                    
                </div>

                <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                    <label>Lastname</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </span>
                        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
                    </div>                        
                    @if ($errors->has('lastname'))
                    <span class="help-block">{{ $errors->first('lastname') }}</span>
                    @endif                    
                </div>            

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label">Email</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                    @endif
                    
                </div>

                
                <div class="form-group"{{ $errors->has('password') ? ' has-error' : '' }}>
                    <label>Password</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-lock"></i>
                        </span>
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                    @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                    @endif                    
                </div>                
                
                
            </div>
            <div class="form-actions">
                <button type="submit" class="btn blue">Save</button>
                <a href="{{ url('/admin/users') }}" class="btn btn-default">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection
