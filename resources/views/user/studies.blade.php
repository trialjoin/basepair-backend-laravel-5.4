@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>Studies</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">Studies</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <div class="bootstrap-table">
            
            
            <table class="table">
                <thead>
                    <tr>
                        <th>NCT</th>
                        <th>Sponsors</th>
                        <th>First Received Date</th>
                        <th>Last Changed Date</th>
                        <th>Completion date</th>
                        <th>Baseline Population</th>
                        <th>Official title</th>
                        <th>Phase</th>
                        <th>Brief title</th>
                        <th>Enrollment</th>
                        <th>Enrollment type</th>
                        <th>Eligibility</th>
                        <th>Overall Official</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($items))
                        @foreach ($items as $item)
                        <tr>
                            <td>{{$item->nct_id}}</td>    
                            <td></td>
                            <td>{{$item->first_received_date}}</td>                            
                            <td>{{$item->last_changed_date}}</td>                            
                            <td>{{$item->completion_date}}</td>                            
                            <td>{{$item->baseline_population}}</td>
                            <td>{{$item->official_title}}</td>
                            <td>{{$item->phase}}</td>
                            <td>{{$item->brief_title}}</td>
                            <td>{{$item->enrollment}}</td>
                            <td>{{$item->enrollment_type}}</td>
                            <td></td>
                            <td></td>
                        </tr>            
                        @endforeach 
                    @else
                    <tr><td class="text-center" colspan="3">No data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif

@endsection
