@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/{{$prefix}}">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <a href="/{{$prefix}}/facilities">Facilities</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>{{$facility->name}}</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">{{$facility->name}}</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">        
        <p><strong>Conditions:</strong></p>
        <ul class="list-unstyled margin-top-10 margin-bottom-10">
        @foreach($facility->getConditions() as $c)
            <li>
                <i class="fa fa-check"></i> {{$c->name}} 
            </li>                    
        @endforeach
        </ul>
        
        <p><strong>Sponsors:</strong></p>
        <ul class="list-unstyled margin-top-10 margin-bottom-10">
        @foreach($facility->getSponsors() as $c)
            <li>
                <i class="fa fa-check"></i> {{$c->name}} 
            </li>                    
        @endforeach
        </ul>
        
        <p class="margin-top-10"><strong>Studies:</strong></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>NCT</th>
                </tr>
            </thead>
            <tbody> 
                @foreach($facility->studies as $s)
                <tr>
                    <td>{{$s->brief_title}}</td>
                    <td>{{$s->nct_id}}</td>
                </tr> 
                @endforeach
            </tbody>
        </table>
        
        <p class="margin-top-10"><strong>Contacts:</strong></p>
        
            <table class="table">
                <thead>
                    <tr>                        
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Studies</th>
                        <th>Actions<br/>
                            <button type="submit" name="merge" class="btn btn-primary">Merge Selected</button>
                        </th>
                    </tr>
                </thead>
                <tbody>                
                    @foreach($facility->contacts as $c)
                    <tr>                        
                        <td>{{$c->name}}</td>
                        <td>{{$c->email}}</td>
                        <td>{{$c->phone}}</td>
                        <td>{{$c->studies()->count()}}</td>
                    </tr>
                    @endforeach         
                </tbody>
            </table>        
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

@endsection
