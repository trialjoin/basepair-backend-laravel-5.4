@extends('layouts.main')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>Sponsors</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">Sponsors</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <div class="bootstrap-table">
            <div>
                {!! Form::model($filter, [ 'method' => 'get']) !!}
                    <div class="pull-left form-inline">
                        <div class="form-group">
                            <label class="control-label">Show</label>
                            {!! Form::select('i', [10 => 10, 25 => 25, 50 => 50,100 => 100] , null, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) !!}                                    
                        </div>
                    </div>
                    
                    <div class="pull-right form-inline">
                        <div class="form-group">
                            <label class="control-label">Search:</label>
                            {!! Form::text('s', null, ['class' => 'form-control']) !!}                
                        </div>
                    </div>
                </form>
            </div>
            
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>                        
                    </tr>
                </thead>
                <tbody>
                    @if(count($items))
                        @foreach ($items as $item)
                        <tr>
                            <td>{{$item->name}}</td>                            
                        </tr>            
                        @endforeach 
                    @else
                    <tr><td class="text-center">No data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif

@endsection
