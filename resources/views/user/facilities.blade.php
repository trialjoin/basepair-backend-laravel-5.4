@extends('layouts.main')

@section('content')
<?php
    $user = Auth::guard("admin")->user();
    $prefix = "admin";
    if(!$user){
        $prefix = "user";        
    }?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/admin">Home</a>
            <i class="fa fa-circle"></i>
        </li> 
        <li>
            <span>Facilities</span>
        </li>
    </ul>    
</div>

<h1 class="page-title">Facilities</h1>
@include('common.flash-message')
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    
    <div class="portlet-body">
        <div class="bootstrap-table">
            <div>
                {!! Form::model($filter, [ 'method' => 'get']) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">City:</label>
                            {!! Form::text('c', null, ['class' => 'form-control']) !!}                
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">State:</label>
                            {!! Form::text('st', null, ['class' => 'form-control']) !!}                
                        </div>
                    </div>
                    <div class="col-md-4">                        
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="" class="btn btn-danger">Reset</a>
                    </div>
                </div>
                
                    <div class="pull-left form-inline">
                        <div class="form-group">
                            <label class="control-label">Show</label>
                            {!! Form::select('i', [10 => 10, 25 => 25, 50 => 50,100 => 100] , null, ['class' => 'form-control', 'onchange' => 'this.form.submit()']) !!}                                    
                        </div>
                    </div>
                    
                    <div class="pull-right form-inline">                                                
                        <div class="form-group">
                            <label class="control-label">Search:</label>
                            {!! Form::text('s', null, ['class' => 'form-control']) !!}                
                        </div>
                    </div>
                </form>
            </div>
            
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($items))
                        @foreach ($items as $item)
                        <tr>
                            <td>{{$item->name}}</td>                            
                            <td>{{$item->city}}</td>                            
                            <td>{{$item->state}}</td>
                            <td><a href="/{{$prefix}}/facility/{{$item->id}}" class="btn btn-primary">View</a></td>
                        </tr>            
                        @endforeach 
                    @else
                    <tr><td class="text-center" colspan="3">No data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
@if($items)
<nav>    
    {{ $items->links() }}            
</nav>
@endif

@endsection
